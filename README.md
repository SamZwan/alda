# ALDA
development of the Automatic Leakage Detection Application (ALDA). This is a software with two different approaches to compute the location and magnitude of a leakage in a water distribution network using a steady state hydraulic model. 


"""
// Copyright (C) 2015
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Automatic Leakage Detection Application (ALDA)
author: Rodolfo Alvarado Montero
email: rodolfo.alvaradoMontero@deltares.nl
first created on November 2017

v1 - approach based on numerical approximation using global heuristic optimization
v2 - approach based on data assimilation using Ensemble Kalman Filters
"""
