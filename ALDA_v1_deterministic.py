# -*- coding: utf-8 -*-
"""
// Copyright (C) 2015
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Automatic Leakage Detection Application (ALDA)
author: Rodolfo Alvarado Montero
email: rodolfo.alvaradoMontero@deltares.nl
first created on November 2017
"""

import numpy as np
import pandas as pd
from pyswarm import pso
import time

#parameters
n = 1.852 # Hazen-Williams exponent
err_NR = 1e-8 # error of iterations of Newton-Raphson method
detect = True

#id,elevation,lateral,boolean leakages (0 for no leakage, 1 for leakage)
nodes = np.array([
        [1,0.0,0.0,1],
        [2,0.0,0.0,1],
        [3,0.0,0.0,1],
        [4,0.0,0.0,1],
        [5,0.0,0.0,1],
        [6,0.0,0.005,1],
        ])

#id,Nup,Ndw,r1,r2,r3, r4
connectors = np.array([
    #deterministic model
    [0,	0,	1, 15651.8],
    [1,	1,	2, 15651.8],
    [2,	2,	3, 15651.8],
    [3,	1,	4, 15651.8],
    [4,	2,	5, 15651.8],
    [5,	3,	6, 15651.8],
    [6,	4,	5, 15651.8],
    [7,	5,	6, 15651.8],

    #original model and very small perturbations
        # [0,	0,	1, 15651.8, 15582.8, 15697.8,	15671.8],
        # [1,	1,	2, 15651.8, 15556.8, 15726.8,	15612.8],
        # [2,	2,	3, 15651.8, 15731.8, 15655.8,	15704.8],
        # [3,	1,	4, 15651.8, 15740.8, 15671.8,	15663.8],
        # [4,	2,	5, 15651.8, 15686.8, 15684.8,	15577.8],
        # [5,	3,	6, 15651.8, 15755.8, 15661.8,	15598.8],
        # [6,	4,	5, 15651.8, 15614.8, 15684.8,	15756.8],
        # [7,	5,	6, 15651.8, 15582.8, 15599.8,	15585.8],

        # original model and normal perturbations between 95-105 Chezy
        # [0, 0, 1, 15651.8, 14831.5, 16342.5, 14746.2, 14379.9, 14347.1],
        # [1, 1, 2, 15651.8, 14303.4, 14581.6, 14899.1, 16943.0, 15481.2],
        # [2, 2, 3, 15651.8, 16153.3, 17066.8, 16454.0, 15720.9, 14837.6],
        # [3, 1, 4, 15651.8, 15119.0, 16151.1, 15570.7, 16765.3, 15857.9],
        # [4, 2, 5, 15651.8, 15469.7, 14883.6, 15346.1, 15507.5, 16397.2],
        # [5, 3, 6, 15651.8, 16546.5, 14424.9, 16870.3, 15862.7, 16903.9],
        # [6, 4, 5, 15651.8, 15783.6, 16692.7, 14625.8, 16692.0, 15483.8],
        # [7, 5, 6, 15651.8, 15883.2, 16303.1, 14638.1, 15138.0, 16038.8],
])

prob_model = np.array([0.75,0.05,0.05,0.05,0.05,0.05])

#id, elevation, pressure
reservoirs = np.array([
        [0,0,30],
        ])

#pressure observations
# p_obs_value = [28.4,27.79,27.63,28.10,27.79,27.47]
# p_obs_index = [1,2,3,4,5,6]       #pressure node ids
# p_obs_value = [27.79,27.63,28.10]
# p_obs_index = [2,3,4]       #pressure node ids
# p_obs_value = [27.63,28.10]
# p_obs_index = [3,4]       #pressure node ids
p_obs_value = [28.10]
p_obs_index = [4]       #pressure node ids

#discharge observations
q_obs_value = []
q_obs_index = []       #discharge ids

#basic variables
Nodes = np.size(nodes,0)
Connectors = np.size(connectors,0)
Reservoirs = np.size(reservoirs,0)
dfNodes = pd.DataFrame(nodes)
dfConnectors = pd.DataFrame(connectors)
dfReservoirs = pd.DataFrame(reservoirs)
dfJunctions = pd.concat([dfNodes.loc[:,0:1],dfReservoirs.loc[:,0:1]], ignore_index = True)
numberModels = np.size(connectors,1) - 3

# preprocessing of elevations to be added to the connectors dataframe
tempUp = np.empty((Connectors,1))
tempDw = np.empty((Connectors,1))
for i in range(Connectors):
    tempUp[i] = dfJunctions.loc[dfJunctions[0] == connectors[i,1], 1]
    tempDw[i] = dfJunctions.loc[dfJunctions[0] == connectors[i,2], 1]
dfConnectors[3+numberModels] = tempUp  #careful: this modifies the array connectors!
dfConnectors[4+numberModels] = tempDw  #careful: this modifies the array connectors!

def simulate(leakages, model):

    # initial values - simulation can be sensitive to initial guess!
    pressure = np.ones((Nodes+Reservoirs,1))*1.0
    discharge = np.ones((Connectors,1))*1.0
    sol = np.r_[pressure, discharge]
    F = np.ones((Nodes+Reservoirs+Connectors,1)) #made of ones so that it can initialize
    dF = np.zeros((Nodes+Reservoirs+Connectors,Nodes+Reservoirs+Connectors)) # [dF/dP, dF/dQ]
    while np.max(F) > err_NR:
        eq = 0
        # energy
        for j in range(Connectors):
            F[eq] = pressure[int(connectors[j,1])] + dfConnectors.loc[j,3+numberModels] - \
                    pressure[int(connectors[j,2])] - dfConnectors.loc[j,4+numberModels] - \
                    connectors[j,3+model]*abs(discharge[int(connectors[j,0])])**(n-1)*discharge[int(connectors[j,0])]
            dF[eq,int(connectors[j,1])] = 1.0
            dF[eq,int(connectors[j,2])] = -1.0
            dF[eq,int(Nodes)+int(Reservoirs)+int(connectors[j,0])] = -1.0*n*connectors[j,3+model]*abs(discharge[int(connectors[j,0])])**(n-2)*discharge[int(connectors[j,0])]
            eq = eq + 1
    
        #continuity
        i_leakages = 0
        for j in range(Nodes):
            matchUp = dfConnectors.loc[dfConnectors[1] == nodes[j,0]]
            matchDw = dfConnectors.loc[dfConnectors[2] == nodes[j,0]]
            SumUp = 0.0
            SumDw = 0.0
            for k in matchUp[0]:                
                SumUp = SumUp + discharge[int(k)]    #check if this makes sense!
                dF[eq,int(Nodes)+int(Reservoirs)+int(k)] = -1.0
            for k in matchDw[0]:
                SumDw = SumDw + discharge[int(k)]
                dF[eq,int(Nodes)+int(Reservoirs)+int(k)] = 1.0
            if nodes[j,3] == 1:
                F[eq] = SumDw - SumUp - nodes[j,2] - leakages[i_leakages]
                i_leakages = i_leakages + 1
            elif nodes[j,3] == 0:
                F[eq] = SumDw - SumUp - nodes[j,2]
            eq = eq + 1

        #reservoirs
        for j in range(Reservoirs):
            F[eq] = pressure[reservoirs[j,0]] - reservoirs[j,2]
            dF[eq,reservoirs[j,0]] = 1
            eq = eq + 1
    
        #solve the system
        sol = sol - np.dot(np.linalg.inv(dF),F)
        pressure = sol[0:Nodes+Reservoirs]
        discharge = sol[Nodes+Reservoirs:]
    return [pressure, discharge]

def minimize_heads(leakages):
    objective = 0.0
    for m in range(numberModels):
        [pressure, discharge] = simulate(leakages, m)
        for i in range(len(p_obs_value)):
            objective += prob_model[m]*abs((p_obs_value[i] - pressure[p_obs_index[i]])/p_obs_value[i])*100
        for i in range(len(q_obs_value)):
            objective += prob_model[m]*abs((q_obs_value[i] - discharge[q_obs_index[i]])/q_obs_value[i])*100
    print ([objective, leakages])
    return objective
     
def tic():
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    if 'startTime_for_tictoc' in globals():
        print ("Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")
    else:
        print ("Toc: start time not set")


l_size = np.count_nonzero(nodes[:,3] == 1) # number of leakages

if detect:
    tic()
    lb = [0.00]*l_size
    ub = [0.01]*l_size
    xopt, fopt = pso(minimize_heads, lb, ub, ieqcons=[], f_ieqcons=None, args=(), kwargs={},
        swarmsize=250, omega=0.5, phip=0.5, phig=0.5, maxiter=200, minstep=1e-6,
        minfunc=1e-4, debug=False)
    leakages = xopt
    toc()

    print ('************* ENDED *************')
    print ('objective = %s' % (fopt))
    print ('leakages = %s' % (leakages))
    for m in range(numberModels):
        [pressure, discharge] = simulate(leakages, m)
        print ('model = %s' %(m))
        print ('pressure = %s' %(pressure))
        print ('discharge = %s' %(discharge))

else:
    print ('************* ENDED *************')
    for m in range(numberModels):
        leakages = np.zeros((l_size,1))
        [pressure, discharge] = simulate(leakages, m)
        print ('model = %s' %(m))
        print ('pressure = %s' %(pressure))
        print ('discharge = %s' %(discharge))
