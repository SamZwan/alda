'''Asala 20.5.2019 Independent Simulation Function for creating observed data based on ficitious leakage scenario '''

import os, pprint
pp = pprint.PrettyPrinter()  # we'll use this later.
from epanettools.epanettools import EPANetSimulation, Node  # import all elements needed
from epanettools.examples import simple  # this is just to get the path of standard examples
file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')  # open an example
es = EPANetSimulation(file)

import os
from epanettools import epanet2 as et
from epanettools.examples import simple
file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')
ret = et.ENopen(file, "Net1.rpt", "")

import numpy as np

es.run()
p=Node.value_type['EN_PRESSURE']
d=Node.value_type['EN_DEMAND']
h=Node.value_type['EN_HEAD']
ret, Nnodes = et.ENgetcount(et.EN_NODECOUNT)

s = (9,24)
demand_obs = np.zeros(s)
head_obs = np.zeros(s)
pressure_obs = np.zeros(s)

def obs_result():

    for j in range(0,24):
        for i in range(0,Nnodes-2):

            demand_obs[i][j] = "%.3f" % es.network.nodes[i+1].results[d][j+1]
            head_obs[i][j] = "%.3f" % es.network.nodes[i+1].results[h][j+1]
            pressure_obs[i][j] = "%.3f" % es.network.nodes[i+1].results[p][j+1]

    return(demand_obs,head_obs,pressure_obs)

obs_result()
(dem,head,pres)= obs_result()
