"""
    ALDA - Automatic Leakage Detection Application
    This ALDA package was writen by Asala Mahajna under the supervision of Sam van der Zwan and Rodolfo Alvarado Montero
    as part of her master's thesis project at Delatres.
    This package utilizes EPANETTOOLS package
    Also, this package uses the bugged byswarm package(this package has been greatly manually edited)
 """

from multiprocessing import Process
import queue
q = queue.Queue()  # For putting and getting results of thread

import pickle

import numpy as np

from Sec_PSO_3 import psos

global demand_obs, head_obs, pressure_obs, e, f, index
# Calling Simulation function for outputting measure hydraulic data
from Simulation import obs_result
(demand_obs, head_obs, pressure_obs) = obs_result()

k = np.float(50)
kk = np.float(0)
kkk= np.float(0)
index = 0
e = []
f = []



def sim_opt():
    #The results of the first sim-opt is taken from the main

    # Importing the simulation function which simulates with input optimal solution and gives objective function value

    from sim_SO_MP_3 import sim

    # Here we assert the dimensions and values of the upper and lower boundaries of the search variables in the O.F
    lb = np.zeros(3, dtype=np.float)
    ub = np.zeros(3, dtype=np.float)
    len(lb) == len(ub)
    lb[0] = float(0)
    lb[1] = float(0.0)
    lb[2] = float(0.0)
    ub[0] = float(400)
    ub[1] = float(300)
    ub[2] = float(200)

    with open("swarmx.pkl","rb") as f:
        xoptin = pickle.load(f)
    with open("swarmf.pkl", "rb") as f:
        foptin = pickle.load(f)
    with open("swarmi.pkl", "rb") as f:
        i = pickle.load(f)

    w = 0.5 * 0.001 * i

    # Now, we call the optimizer
    xopt, fopt, status = psos(sim, lb, ub, xoptin, foptin,  swarmsize=98, omega=w, phip=0.5, phig=0.5, maxiter=0, minstep=0.01, minfunc=0.01, minfunclim = 300)


    i = i+1
    print(i)
    # we update the pickled values of swarm best position and cost (xopt, fopt) and status (if True optimization end if false optimization ends)
    with open("swarmx.pkl", "wb") as f:
        pickle.dump(xopt, f)

    with open("swarmf.pkl", "wb") as f:
        pickle.dump(fopt, f)

    with open("swarms.pkl", "wb") as f:
        pickle.dump(status, f)

    with open("swarmi.pkl", "wb") as f:
        pickle.dump(i, f)

    return xopt, fopt, status

if __name__ == '__main__':

    import os, pprint
    pp = pprint.PrettyPrinter()  # we'll use this later.
    from epanettools.epanettools import EPANetSimulation, Node, Link, Network, Nodes, Links, Patterns, Pattern, \
        Controls, Control  # import all elements needed
    from epanettools.examples import simple  # this is just to get the path of standard examples
    file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')  # open an example
    es = EPANetSimulation(file)
    import os
    from epanettools import epanet2 as et
    from epanettools.examples import simple
    file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')
    ret = et.ENopen(file, "Net1.rpt", "")
    import tempfile, os
    import numpy as np
    from PSO_Asala import pso
    import time
    import math
    from Simulation import obs_result
    (demand_obs, head_obs, pressure_obs) = obs_result()
    k = np.float(50)
    kk = np.float(0)
    kkk= np.float(0)
    # I run "simulation" to get the measured/ observed values of the hydraulic parameters
    # I send the nodal demand less and insert in k to create a scenario in which there is fake leak
    e = [0] * 500
    f = [0] * 500

    def sim_opt(dt):

        tot = 0
        dta = np.float(dt[0])
        dtb = np.float(dt[1])
        dtc = np.float(dt[2])
        global index, demand_obs, head_obs, pressure_obs, e, f
        d = Node.value_type['EN_BASEDEMAND']
        es.ENgetnodevalue(6, d)[1]  # low level interface
        es.network.nodes[6].results[d]  # new interface
        es.ENgetnodevalue(2, d)[1]  # low level interface
        es.network.nodes[2].results[d]
        es.ENgetnodevalue(9, d)[1]  # low level interface
        es.network.nodes[9].results[d]

        r = es.ENsetnodevalue(6, d, float(dta))  # now let's change values - link
        rr = es.ENsetnodevalue(2, d, float(dtb))
        rrr = es.ENsetnodevalue(9, d, float(dtc))
        f[index] = os.path.join(tempfile.gettempdir(), "temp.inp")
        es.ENsaveinpfile(f[index])  # save the changed file
        r = es.ENsaveinpfile(f[index])
        e[index] = EPANetSimulation(f[index])
        e[index].run()
        p = Node.value_type['EN_PRESSURE']
        h = Node.value_type['EN_HEAD']
        de = Node.value_type['EN_DEMAND']
        ret, Nnodes = et.ENgetcount(et.EN_NODECOUNT)
        s = (9, 24)
        demand_sim = np.zeros(s)
        head_sim = np.zeros(s)
        pressure_sim = np.zeros(s)

        for j in range(0, 24):
            for i in range(0, Nnodes - 2):
                demand_sim[i][j] = "%.3f" % e[index].network.nodes[i + 1].results[de][j + 1]
                pressure_sim[i][j] = "%.3f" % e[index].network.nodes[i + 1].results[p][j + 1]
                head_sim[i][j] = "%.3f" % e[index].network.nodes[i + 1].results[h][j + 1]

        for j in range(0, 24):
            for i in range(0, Nnodes - 2):
                tot += math.fabs(np.float(demand_obs[i][j]) - np.float(demand_sim[i][j])) + math.fabs(
                    np.float(pressure_obs[i][j]) - np.float(pressure_sim[i][j])) + math.fabs(
                    np.float(head_obs[i][j]) - np.float(head_sim[i][j]))

        index += 1
        return tot


    def tic():
        global startTime_for_tictoc
        startTime_for_tictoc = time.time()


    def toc():
        if 'startTime_for_tictoc' in globals():
            print("Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")
        else:
            print("Toc: start time not set")


    tic()

    lb = np.zeros(3, dtype=np.float)
    ub = np.zeros(3, dtype=np.float)
    len(lb) == len(ub)
    lb[0] = float(0)
    lb[1] = float(0.0)
    lb[2] = float(0.0)
    ub[0] = float(400)
    ub[1] = float(300)
    ub[2] = float(200)

    xopt = np.zeros(3, dtype=np.float)
    fopt = np.zeros(1, dtype=np.float)

    xopt, fopt, status = pso(sim_opt, lb, ub,
                     swarmsize=24, omega=10, phip=10, phig=10, maxiter=2, minstep=0.01,
                     minfunc=0.01, minfunclim =300)

    with open("swarms.pkl", "wb") as f:
        pickle.dump(status, f)

    with open("swarmx.pkl", "wb") as f:
        pickle.dump(xopt, f)

    with open("swarmf.pkl", "wb") as f:
        pickle.dump(fopt, f)

    print('objective  = %.3f or The next swarm best cost for initializing next iteration' % (fopt))
    c = np.float(xopt[0]) - float(k)
    cc = np.float(xopt[1]) - float(kk)
    ccc = np.float(xopt[2]) - float(kkk)
    print('The leakage at node 22 with index 6  = %.3f' % (c))
    print('The leakage at node 22 with index 2  = %.3f' % (cc))
    print('The leakage at node 22 with index 9  = %.3f' % (ccc))

    processes = []

    iter = 2
    with open("swarmi.pkl", "wb") as f:
        pickle.dump(iter, f)


    with open("swarms.pkl", "rb") as f:
        status = pickle.load(f)

    while status == False:



        process = Process(target=sim_opt, args=())
        processes.append(process)
        process.start()


        for process in processes:
            process.join()
            with open("swarms.pkl", "rb") as f:
                status = pickle.load(f)


    with open("swarmx.pkl","rb") as f:
        xopt = pickle.load(f)

    dev = np.abs(xopt[0] - 200) + np.abs(xopt[1] - 150)+ np.abs(xopt[2] - 100)

    print('The deviation from accurate detection is {:}'.format(dev))
    print('************* ENDED *************')
    toc()






