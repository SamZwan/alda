import numpy as np



def psos(func, lb, ub, xoptin, foptin, swarmsize=19, omega=0.5, phip=0.5, phig=0.5, maxiter=5,
            minstep=5, minfunc=10, minfunclim =10 ):


    """
    Perform a particle swarm optimization (PSO)

    Parameters
    ==========
    func : function
        The function to be minimized
    lb : array
        The lower bounds of the design variable(s)
    ub : array
        The upper bounds of the design variable(s)

    Optional
    ========
    swarmsize : int
        The number of particles in the swarm (Default: 100)
    omega : scalar
        Particle velocity scaling factor (Default: 0.5)
    phip : scalar
        Scaling factor to search away from the particle's best known position
        (Default: 0.5)
    phig : scalar
        Scaling factor to search away from the swarm's best known position
        (Default: 0.5)
    maxiter : int
        The maximum number of iterations for the swarm to search (Default: 100)
    minstep : scalar
        The minimum stepsize of swarm's best position before the search
        terminates (Default: 1e-8)
    minfunc : scalar
        The minimum change of swarm's best objective value before the search
        terminates (Default: 1e-8)
    minfunclim: scalar
        This is the upper limit for satisfying the objective function goal


    Returns
    =======
    g : array
        The swarm's best known position (optimal design)
    f : scalar
        The objective value at ``g``
    status: boolean
        It returns True once the optimization conditions are satisfied,
        Otherwise, it returns False which keeps the process going

    """
    status = False



    assert len(lb) == len(ub), 'Lower- and upper-bounds must be the same length'
    assert hasattr(func, '__call__'), 'Invalid function handle'
    lb = np.array(lb)
    ub = np.array(ub)
    assert np.all(ub > lb), 'All upper-bound values must be greater than lower-bound values'

    vhigh = np.abs(ub - lb)
    vlow = -vhigh

    # Check for constraint function(s) #########################################
    obj = lambda x: func(x)


    # Initialize the particle swarm ############################################
    S = swarmsize
    D = len(lb)  # the number of dimensions each particle has
    x = np.random.uniform(lb,ub,size=(S, D))  # particle positions
    v = np.zeros_like(x)  # particle velocities
    p = np.zeros_like(x)  # best particle positions
    fp = np.zeros(S)  # best particle function values


    for i in range(S):
        # Initialize the particle's position
        p[i, :] = xoptin

        # Calculate the objective's value at the current particle's
        fp[i] = foptin

        # Initialize the particle's velocity
        v[i, :] = vlow + np.random.rand(D) * (vhigh - vlow)


    # Iterate until termination criterion met ##################################

    it = 0
    while it<=maxiter:
        rp = np.random.rand(S, D)
        rg = np.random.rand(S, D)

        for i in range(S):

            # Update the particle's velocity
            v[i, :] = omega * v[i, :] + phip * rp[i, :] * (p[i, :] - x[i, :]) + \
                      phig * rg[i, :] * (xoptin - x[i, :])

            # Update the particle's position, correcting lower and upper bound
            # violations, then update the objective function value
            x[i, :] = x[i, :] + v[i, :]

            mark1 = x[i, :] < lb
            mark2 = x[i, :] > ub
            x[i, mark1] = lb[mark1]
            x[i, mark2] = ub[mark2]


            fx = obj(x[i, :])

            if fx <= minfunclim:
                status = True
                tmp = x[i, :].copy()
                print('Stopping search: Swarm best objective less than {:}'.format(minfunclim))
                print('Swarm best objective is {:}'.format(fx))
                print('The search variable value{:}'.format(tmp))
                return tmp, fx, status

            elif fx < fp[i]:
                p[i, :] = x[i, :].copy()
                fp[i] = fx
                if fx < foptin:
                    tmp = p[i, :].copy()

                    if np.abs(foptin - fx) <= minstep:
                        print('Stopping search: Swarm best objective change less than {:}'.format(minfunc))
                        status = True
                        return tmp, fx, status
                    elif  fx <=  minfunc:
                        print('Stopping search: Swarm best position change less than {:}'.format(minstep))
                        status = True
                        return tmp, fx, status
                    else:
                        g = p[i, :].copy()
                        fg = fx
                        return g, fg, status
            else:
                g = xoptin
                fg = foptin



        it += 1

    print('Stopping search: maximum iterations reached --> {:}'.format(maxiter))
    print('Stopping search: The next swarm best position for initializing next iteration --> {:}'.format(g))
    print('Stopping search: The next swarm best cost for initializing next iteration --> {:}'.format(fg))
    return g, fg, status

